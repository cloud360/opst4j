package com.cloudbase.opst4j.openstack.service.network;

import com.cloudbase.opst4j.api.network.NetworkService;
import com.cloudbase.opst4j.domain.network.Network;
import com.cloudbase.opst4j.domain.network.NetworkQuota;
import com.cloudbase.opst4j.domain.network.SecureGroup;
import com.cloudbase.opst4j.http.HttpCommand;
import com.cloudbase.opst4j.http.HttpExecutor;
import com.cloudbase.opst4j.http.HttpMethod;
import com.cloudbase.opst4j.openstack.domain.network.NeutronNetwork;
import com.cloudbase.opst4j.openstack.domain.network.NeutronNetworks;
import com.cloudbase.opst4j.openstack.domain.network.NeutronSecureGroup;
import com.cloudbase.opst4j.openstack.domain.network.NeutronSecureGroups;

import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/1/14
 */
public class NeutronNetworkService implements NetworkService {

    private final HttpExecutor executor;

    private final String endpoint;

    public NeutronNetworkService(HttpExecutor executor, String endpoint) {
        this.executor = executor;
        this.endpoint = endpoint;
    }

    @Override
    public List<Network> listNetworks(String access_token) {
        HttpCommand<NeutronNetworks> command = HttpCommand.build(NeutronNetworks.class)
                .endpoint(endpoint)
                .path("networks")
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        return executor.execute(command, null).networks();
    }

    @Override
    public Network detailNetwork(String id, String access_token) {
        HttpCommand<NeutronNetwork> command = HttpCommand.build(NeutronNetwork.class)
                .endpoint(endpoint)
                .path("networks/" + id)
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        return executor.execute(command, null);
    }

    @Override
    public NetworkQuota quotas(String access_token) {
        HttpCommand<NetworkQuota> command = HttpCommand.build(NetworkQuota.class)
                .endpoint(endpoint)
                .path("quotas")
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        return executor.execute(command, null);
    }

    @Override
    public List<SecureGroup> listSecureGroups(String access_token) {
        HttpCommand<NeutronSecureGroups> command = HttpCommand.build(NeutronSecureGroups.class)
                .endpoint(endpoint)
                .path("security-groups")
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        NeutronSecureGroups nsgs = executor.execute(command, null);
        List<SecureGroup> l = new LinkedList<>();
        if (nsgs != null) {
            l.addAll(nsgs.security_groups);
        }

        return l;
    }

    @Override
    public SecureGroup detailSecureGroup(String secGroupId, String access_token) {
        return _detailSecureGroup(secGroupId, access_token);
    }

    private NeutronSecureGroup _detailSecureGroup(String secGroupId, String access_token) {
        HttpCommand<NeutronSecureGroup> command = HttpCommand.build(NeutronSecureGroup.class)
                .endpoint(endpoint)
                .path("security-groups/" + secGroupId)
                .token(access_token)
                .method(HttpMethod.GET)
                .build();

        return executor.execute(command, null);
    }
}
