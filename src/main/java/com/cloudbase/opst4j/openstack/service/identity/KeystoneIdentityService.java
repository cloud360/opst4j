package com.cloudbase.opst4j.openstack.service.identity;

import com.cloudbase.opst4j.api.identity.IdentityService;
import com.cloudbase.opst4j.domain.identity.Access;
import com.cloudbase.opst4j.http.HttpCommand;
import com.cloudbase.opst4j.http.HttpExecutor;
import com.cloudbase.opst4j.http.HttpMethod;
import com.cloudbase.opst4j.openstack.domain.identity.Credentials;
import com.cloudbase.opst4j.openstack.domain.identity.KeystoneAccess;
import com.cloudbase.opst4j.openstack.domain.identity.TokenCredentials;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/30/14
 */
public class KeystoneIdentityService implements IdentityService {

    private final HttpExecutor executor;

    private final String endpoint;

    public KeystoneIdentityService(HttpExecutor executor, String endpoint) {
        this.executor = executor;
        this.endpoint = endpoint;
    }

    @Override
    public Access token(String tenant, String username, String password) {
        HttpCommand.HttpCommandBuilder<KeystoneAccess> b = HttpCommand.build(KeystoneAccess.class);

        HttpCommand<KeystoneAccess> cmd = b.endpoint(endpoint)
                .path("tokens")
                .method(HttpMethod.POST)
                .build();

        return executor.execute(cmd, new Credentials(tenant, username, password));
    }

    @Override
    public Access refreshToken(String tenant, String current_token) {
        HttpCommand<KeystoneAccess> cmd = HttpCommand.build(KeystoneAccess.class)
                .path("tokens")
                .method(HttpMethod.POST)
                .build();

        return executor.execute(cmd, new TokenCredentials(tenant, current_token));
    }
}
