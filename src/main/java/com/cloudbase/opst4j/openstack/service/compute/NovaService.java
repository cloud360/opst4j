package com.cloudbase.opst4j.openstack.service.compute;

import com.cloudbase.opst4j.api.compute.ComputeService;
import com.cloudbase.opst4j.api.image.ImageService;
import com.cloudbase.opst4j.api.network.NetworkService;
import com.cloudbase.opst4j.domain.compute.ComputeLimits;
import com.cloudbase.opst4j.domain.compute.Flavor;
import com.cloudbase.opst4j.domain.compute.NetworkAddresses;
import com.cloudbase.opst4j.domain.compute.Server;
import com.cloudbase.opst4j.domain.compute.action.ServerCreate;
import com.cloudbase.opst4j.domain.compute.action.ServerCreateRes;
import com.cloudbase.opst4j.domain.compute.action.ServerRebuild;
import com.cloudbase.opst4j.http.HttpCommand;
import com.cloudbase.opst4j.http.HttpExecutor;
import com.cloudbase.opst4j.http.HttpMethod;
import com.cloudbase.opst4j.http.exception.ServerInternalException;
import com.cloudbase.opst4j.http.exception.UnauthorizedException;
import com.cloudbase.opst4j.openstack.domain.compute.NovaComputeLimits;
import com.cloudbase.opst4j.openstack.domain.compute.NovaFlavor;
import com.cloudbase.opst4j.openstack.domain.compute.NovaFlavors;
import com.cloudbase.opst4j.openstack.domain.compute.NovaNetworkAddresses;
import com.cloudbase.opst4j.openstack.domain.compute.NovaServer;
import com.cloudbase.opst4j.openstack.domain.compute.NovaServers;
import com.cloudbase.opst4j.openstack.domain.compute.action.NovaServerCreate;
import com.cloudbase.opst4j.openstack.domain.compute.action.NovaServerCreateRes;

import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/30/14
 */
public class NovaService implements ComputeService {

    private final HttpExecutor executor;

    private final String endpoint;

    private final ImageService imageService;

    private final NetworkService networkService;

    //TODO: Use Guice and assisted inject to inject dependencies
    public NovaService(HttpExecutor executor, ImageService imageService, NetworkService networkService, String endpoint) {
        this.executor = executor;
        this.imageService = imageService;
        this.networkService = networkService;
        this.endpoint = endpoint;
    }

    @Override
    public ImageService imageService() {
        return imageService;
    }

    @Override
    public NetworkService networkService() {
        return networkService;
    }

    @Override
    public List<Server> detailServers(String tenantId, String access_token) {
        HttpCommand<NovaServers> command = HttpCommand.build(NovaServers.class)
                .endpoint(endpoint)
                .path(tenantId + "/servers/detail")
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        NovaServers nvs = executor.execute(command, null);

        //Non-optimal solution to get list of detailed instances
        List<Server> l = new LinkedList<>();
        for (NovaServer nv : nvs.servers) {
            try {
                NovaServer nd = _detailServer(tenantId, nv.id, access_token);
                l.add(nd);
            } catch (Exception ex) {
                l.add(nv);
                ex.printStackTrace();
            }
        }

        return l;
    }

    @Override
    public Server detailServer(String tenantId, String server_id, String access_token) {
        return _detailServer(tenantId, server_id, access_token);
    }

    public NovaServer _detailServer(String tenantId, String server_id, String access_token) {
        HttpCommand<NovaServer> command = HttpCommand.build(NovaServer.class)
                .endpoint(endpoint)
                .path(tenantId + "/servers/" + server_id)
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        NovaServer ns = executor.execute(command, null);
        try {
            NovaFlavor nvf = _detailFlavor(tenantId, ns.flavor.id, access_token);
            ns.flavor = nvf;
        } catch (Exception ex) {
            //TODO: User logger
            ex.printStackTrace();
        }

        return ns;
    }

    @Override
    public ComputeLimits limits(String tenantId, String access_token) {
        HttpCommand<NovaComputeLimits> command = HttpCommand.build(NovaComputeLimits.class)
                .endpoint(endpoint)
                .path(tenantId + "/limits")
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        return executor.execute(command, null);
    }

    @Override
    public List<Flavor> detailFlavors(String tenantId, String access_token) {
        HttpCommand<NovaFlavors> command = HttpCommand.build(NovaFlavors.class)
                .endpoint(endpoint)
                .path(tenantId + "/flavors/detail")
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        NovaFlavors nvfs = executor.execute(command, null);

        List<Flavor> l = new LinkedList<>();
        for (NovaFlavor f : nvfs.flavors) {
            try {
                NovaFlavor n = _detailFlavor(tenantId, f.id, access_token);
                l.add(n);
            } catch (Exception ex) {
                l.add(f);
                ex.printStackTrace();
            }
        }

        return l;
    }

    @Override
    public Flavor detailFlavor(String tenantId, String flavor_id, String access_token) {
        return _detailFlavor(tenantId, flavor_id, access_token);
    }

    private NovaFlavor _detailFlavor(String tenantId, String flavor_id, String access_token) {
        HttpCommand<NovaFlavor> command = HttpCommand.build(NovaFlavor.class)
                .endpoint(endpoint)
                .path(tenantId + "/flavors/" + flavor_id)
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        return executor.execute(command, null);
    }

    @Override
    public NetworkAddresses networkAddresses(String tenantId, String serverId, String networkLabel, String access_token) {
        HttpCommand<NovaNetworkAddresses> command = HttpCommand.build(NovaNetworkAddresses.class)
                .endpoint(endpoint)
                .path(tenantId + "/servers/" + serverId + "/ips/" + networkLabel)
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        return executor.execute(command, null);
    }

    @Override
    public ServerCreateRes createServer(String tenantId, String access_token, ServerCreate sc) {
        HttpCommand<String> command = HttpCommand.build(String.class)
                .endpoint(endpoint)
                .path(tenantId + "/servers")
                .method(HttpMethod.POST)
                .token(access_token)
                .build();

        try {
            return new NovaServerCreateRes(200, executor.execute(command, new NovaServerCreate(sc)));
        } catch (UnauthorizedException unauthEx) {
            return new NovaServerCreateRes(403, unauthEx.getMessage());
        } catch (ServerInternalException svEx) {
            return new NovaServerCreateRes(502, svEx.getMessage());
        }
    }

    @Override
    public void rebootServer(String tenantId, String serverId, String access_token) {
        HttpCommand<Void> command = HttpCommand.build()
                .endpoint(endpoint)
                .path(tenantId + "/servers/" + serverId + "/action")
                .method(HttpMethod.POST)
                .token(access_token)
                .build();

        executor.execute(command, "{ \"reboot\": { \"type\": \"SOFT\"} }");
    }

    @Override
    public Server rebuildServer(String tenantId, String serverId, String access_token, ServerRebuild sr) {
        return null;
    }

    @Override
    public void resizeServer(String tenantId, String serverId, String flavorRef, String access_token) {

    }

    @Override
    public void changePassword(String tenantId, String serverId, String newPass, String acces_token) {
        HttpCommand<Void> command = HttpCommand.build()
                .endpoint(endpoint)
                .path(tenantId + "/servers/" + serverId + "/action")
                .method(HttpMethod.POST)
                .token(acces_token)
                .build();

        executor.execute(command, "{\"changePassword\": {\"adminPass\": \"" + newPass + "\"} }");
    }

    @Override
    public void deleteServer(String tenantId, String serverId, String access_token) {
        HttpCommand<Void> command = HttpCommand.build()
                .endpoint(endpoint)
                .path(tenantId + "/servers/" + serverId)
                .method(HttpMethod.DELETE)
                .token(access_token)
                .build();

        executor.execute(command, null);
    }
}
