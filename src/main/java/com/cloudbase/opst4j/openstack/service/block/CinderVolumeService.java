package com.cloudbase.opst4j.openstack.service.block;

import com.cloudbase.opst4j.api.block.VolumeService;
import com.cloudbase.opst4j.domain.block.Volume;
import com.cloudbase.opst4j.http.HttpCommand;
import com.cloudbase.opst4j.http.HttpExecutor;
import com.cloudbase.opst4j.http.HttpMethod;
import com.cloudbase.opst4j.openstack.domain.block.CinderVolume;
import com.cloudbase.opst4j.openstack.domain.block.CinderVolumes;

import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
public class CinderVolumeService implements VolumeService {

    private final HttpExecutor executor;

    private final String endpoint;

    public CinderVolumeService(HttpExecutor executor, String endpoint) {
        this.endpoint = endpoint;
        this.executor = executor;
    }

    @Override
    public List<Volume> listVolumes(String tenantId, String access_token) {
        HttpCommand<CinderVolumes> command = HttpCommand.build(CinderVolumes.class)
                .endpoint(endpoint)
                .path(tenantId + "/volumes/detail")
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        return executor.execute(command, null).volumes();
    }

    @Override
    public Volume detailVolume(String tenantId, String volumeId, String access_token) {
        HttpCommand<CinderVolume> command = HttpCommand.build(CinderVolume.class)
                .endpoint(endpoint)
                .path(tenantId + "/volumes/" + volumeId)
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        return executor.execute(command, null);
    }
}
