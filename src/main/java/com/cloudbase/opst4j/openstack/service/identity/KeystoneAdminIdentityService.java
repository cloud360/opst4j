package com.cloudbase.opst4j.openstack.service.identity;

import com.cloudbase.opst4j.api.identity.AdminIdentityService;
import com.cloudbase.opst4j.http.HttpExecutor;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/10/14
 */
public class KeystoneAdminIdentityService implements AdminIdentityService {

    private final HttpExecutor executor;

    public KeystoneAdminIdentityService(HttpExecutor executor){
        this.executor = executor;
    }

    @Override
    public int createUser(String username, String password, String admin_token) {
        return 0;
    }
}
