package com.cloudbase.opst4j.openstack.service.image;

import com.cloudbase.opst4j.api.image.ImageService;
import com.cloudbase.opst4j.domain.image.Image;
import com.cloudbase.opst4j.http.HttpCommand;
import com.cloudbase.opst4j.http.HttpExecutor;
import com.cloudbase.opst4j.http.HttpMethod;
import com.cloudbase.opst4j.openstack.domain.image.GlanceImage;
import com.cloudbase.opst4j.openstack.domain.image.GlanceImages;

import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/31/14
 */
public class GlanceImageService implements ImageService {

    private final HttpExecutor executor;

    private final String endpoint;

    public GlanceImageService(HttpExecutor executor, String endpoint) {
        this.executor = executor;
        this.endpoint = endpoint;
    }

    @Override
    public List<Image> listImages(String access_token) {
        HttpCommand<GlanceImages> command = HttpCommand.build(GlanceImages.class)
                .endpoint(endpoint)
                .path("images")
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        return executor.execute(command, null).images();
    }

    @Override
    public Image imageDetail(String id, String access_token) {
        HttpCommand<GlanceImage> command = HttpCommand.build(GlanceImage.class)
                .endpoint(endpoint)
                .path("images/" + id)
                .method(HttpMethod.GET)
                .token(access_token)
                .build();

        return executor.execute(command, null);
    }
}
