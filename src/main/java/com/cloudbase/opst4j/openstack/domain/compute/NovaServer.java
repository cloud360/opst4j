package com.cloudbase.opst4j.openstack.domain.compute;

import com.cloudbase.opst4j.domain.compute.Addresses;
import com.cloudbase.opst4j.domain.compute.Flavor;
import com.cloudbase.opst4j.domain.compute.Image;
import com.cloudbase.opst4j.domain.compute.Server;
import org.codehaus.jackson.map.annotate.JsonRootName;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/30/14
 */
@JsonRootName("server")
public class NovaServer implements Server {

    public String accessIPv4;

    public String accessIPv6;

    public String created;

    public String hostId;

    public String id;

    public String name;

    public int progress;

    public String status;

    public String tenant_id;

    public String updated;

    public String user_id;

    public NovaAddresses addresses;

    public NovaImage image;

    public NovaFlavor flavor;

    @Override
    public String accessIPv4() {
        return accessIPv4;
    }

    @Override
    public String accessIPv6() {
        return accessIPv6;
    }

    @Override
    public String created() {
        return created;
    }

    @Override
    public String hostId() {
        return hostId;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public int progress() {
        return progress;
    }

    @Override
    public String status() {
        return status;
    }

    @Override
    public String tenant_id() {
        return tenant_id;
    }

    @Override
    public String updated() {
        return updated;
    }

    @Override
    public String user_id() {
        return user_id;
    }

    @Override
    public Addresses addresses() {
        return addresses;
    }

    @Override
    public Image image() {
        return image;
    }

    @Override
    public Flavor flavor() {
        return flavor;
    }
}
