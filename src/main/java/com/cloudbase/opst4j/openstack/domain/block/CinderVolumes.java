package com.cloudbase.opst4j.openstack.domain.block;

import com.cloudbase.opst4j.domain.block.Volume;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
public class CinderVolumes implements Serializable {

    public List<CinderVolume> volumes;

    public CinderVolumes() {
    }

    public List<Volume> volumes() {
        List<Volume> l = new LinkedList<>();
        if (volumes != null) {
            l.addAll(volumes);
        }

        return l;
    }
}
