package com.cloudbase.opst4j.openstack.domain.identity;

import org.codehaus.jackson.map.annotate.JsonRootName;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
@JsonRootName("auth")
public class TokenCredentials implements Serializable {

    public String tenantName;

    public TokenHolder token;

    public TokenCredentials() {
    }

    public TokenCredentials(String tenantName, String token) {
        this.tenantName = tenantName;
        this.token = new TokenHolder(token);
    }
}
