package com.cloudbase.opst4j.openstack.domain.image;

import com.cloudbase.opst4j.domain.image.Image;
import org.codehaus.jackson.annotate.JsonProperty;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/31/14
 */
public class GlanceImage implements Image {

    public String status;

    public String name;

    public String id;

    public String created_at;

    public String updated_at;

    public String deleted_at;

    public String disk_format;

    public long min_disk;

    public long min_ram;

    public String checksum;

    public String owner;

    public String file;

    public long size;

    public String schema;

    @JsonProperty("protected")
    public boolean isProtected;

    public String self;

    public String visibility;

    public GlanceImage() {
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String status() {
        return status;
    }

    @Override
    public String created_at() {
        return created_at;
    }

    @Override
    public String deleted_at() {
        return deleted_at;
    }

    @Override
    public String updated_at() {
        return updated_at;
    }

    @Override
    public String owner() {
        return owner;
    }

    @Override
    public long size() {
        return size;
    }

    @Override
    public String file() {
        return file;
    }

    @Override
    public String disk_format() {
        return disk_format;
    }

    @Override
    public String schema() {
        return schema;
    }

    @Override
    public long min_disk() {
        return min_disk;
    }

    @Override
    public long min_ram() {
        return min_ram;
    }

    @Override
    public String checksum() {
        return checksum;
    }

    @Override
    public boolean isProtected() {
        return isProtected;
    }

    @Override
    public String visibility() {
        return visibility;
    }

    public String self(){
        return self;
    }
}
