package com.cloudbase.opst4j.openstack.domain.network;

import com.cloudbase.opst4j.domain.network.SecureGroup;
import com.cloudbase.opst4j.domain.network.SecureGroupRule;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;

import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
@JsonRootName("security_group")
public class NeutronSecureGroup implements SecureGroup {

    public String id;

    public String name;

    public String description;

    public String tenant_id;

    @JsonProperty("security_group_rules")
    public List<NeutronSecureGroupRule> neutronSecGroups;

    @Override
    public String id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public List<SecureGroupRule> secureGroupRules() {
        List<SecureGroupRule> l = new LinkedList<>();
        if (neutronSecGroups != null) {
            l.addAll(neutronSecGroups);
        }

        return l;
    }

    @Override
    public String tenant_id() {
        return tenant_id;
    }
}
