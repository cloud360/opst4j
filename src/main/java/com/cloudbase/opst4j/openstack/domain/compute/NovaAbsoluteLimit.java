package com.cloudbase.opst4j.openstack.domain.compute;

import com.cloudbase.opst4j.domain.compute.AbsoluteLimit;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
public class NovaAbsoluteLimit implements AbsoluteLimit {

    public int maxImageMeta;

    public int maxPersonality;

    public int maxPersonalitySize;

    public int maxSecurityGroupRules;

    public int maxSecurityGroups;

    public int maxServerMeta;

    public int maxTotalCores;

    public int maxTotalFloatingIps;

    public int maxTotalInstances;

    public int maxTotalKeypairs;

    public long maxTotalRAMSize;

    public NovaAbsoluteLimit() {
    }

    @Override
    public int maxImageMeta() {
        return maxImageMeta;
    }

    @Override
    public int maxPersonality() {
        return maxPersonality;
    }

    @Override
    public int maxSecurityGroupRules() {
        return maxSecurityGroupRules;
    }

    @Override
    public int maxSecurityGroups() {
        return maxSecurityGroupRules;
    }

    @Override
    public int maxServerMeta() {
        return maxServerMeta;
    }

    @Override
    public int maxTotalCores() {
        return maxTotalCores;
    }

    @Override
    public int maxTotalFloatingIps() {
        return maxTotalFloatingIps;
    }

    @Override
    public int maxTotalInstances() {
        return maxTotalInstances;
    }

    @Override
    public int maxTotalKeypairs() {
        return maxTotalKeypairs;
    }

    @Override
    public long maxPersonalitySize() {
        return maxPersonalitySize;
    }

    @Override
    public long maxTotalRAMSize() {
        return maxTotalRAMSize;
    }
}
