package com.cloudbase.opst4j.openstack.domain.identity;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/30/14
 */
public class PasswordCredentials implements Serializable {

    public String username;

    public String password;

    public PasswordCredentials() {
    }

    public PasswordCredentials(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
