package com.cloudbase.opst4j.openstack.domain.identity;

import org.codehaus.jackson.annotate.JsonProperty;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
public class TokenHolder implements Serializable {

    @JsonProperty("id")
    public String token;

    public TokenHolder() {
    }

    public TokenHolder(String token) {
        this.token = token;
    }
}
