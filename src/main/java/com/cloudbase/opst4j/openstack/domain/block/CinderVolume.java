package com.cloudbase.opst4j.openstack.domain.block;

import com.cloudbase.opst4j.domain.block.Volume;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
@JsonRootName("volume")
public class CinderVolume implements Volume {

    public String id;

    public String name;

    public String description;

    public String status;

    public long size;

    @JsonProperty("os-vol-host-attr:host")
    public String os_vol_host;

    @JsonProperty("os-vol-tenant-attr:tenant_id")
    public String os_vol_tenant;

    public String bootable;

    public String created_at;

    public String volume_type;

    public String availability_zone;

    public CinderVolume(){}

    @Override
    public String name() {
        return name;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public String status() {
        return status;
    }

    @Override
    public long size() {
        return size;
    }

    @Override
    public String os_vol_host() {
        return os_vol_host;
    }

    @Override
    public String os_vol_tenant() {
        return os_vol_tenant;
    }

    @Override
    public String volume_type() {
        return volume_type;
    }

    @Override
    public String bootable() {
        return bootable;
    }

    @Override
    public String created_at() {
        return created_at;
    }

    @Override
    public String availability_zone() {
        return availability_zone;
    }
}
