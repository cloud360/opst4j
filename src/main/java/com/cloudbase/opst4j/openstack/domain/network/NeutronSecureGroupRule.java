package com.cloudbase.opst4j.openstack.domain.network;

import com.cloudbase.opst4j.domain.network.SecureGroupRule;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
public class NeutronSecureGroupRule implements SecureGroupRule {

    public String id;

    public String direction;

    public String ethertype;

    public int port_range_max;

    public int port_range_min;

    public String protocol;

    public String remote_group_id;

    public String remote_ip_prefix;

    public String security_group_id;

    public String tenant_id;

    public NeutronSecureGroupRule() {
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String direction() {
        return direction;
    }

    @Override
    public String ethertype() {
        return ethertype;
    }

    @Override
    public int port_range_max() {
        return port_range_max;
    }

    @Override
    public int port_range_min() {
        return port_range_min;
    }

    @Override
    public String protocol() {
        return protocol;
    }

    @Override
    public String remote_group_id() {
        return remote_group_id;
    }

    @Override
    public String remote_ip_prefix() {
        return remote_ip_prefix;
    }

    @Override
    public String security_group_id() {
        return security_group_id;
    }

    @Override
    public String tenant_id() {
        return tenant_id;
    }
}
