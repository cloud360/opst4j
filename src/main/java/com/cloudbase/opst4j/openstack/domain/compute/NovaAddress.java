package com.cloudbase.opst4j.openstack.domain.compute;

import com.cloudbase.opst4j.domain.compute.Address;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/1/14
 */
public class NovaAddress implements Address {

    public String addr;

    public int version;

    public NovaAddress() {
    }

    @Override
    public String addr() {
        return addr;
    }

    @Override
    public int version() {
        return version;
    }
}
