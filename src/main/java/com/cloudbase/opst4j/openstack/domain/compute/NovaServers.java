package com.cloudbase.opst4j.openstack.domain.compute;

import com.cloudbase.opst4j.domain.compute.Server;
import org.codehaus.jackson.map.annotate.JsonRootName;

import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/30/14
 */
public class NovaServers {

    public List<NovaServer> servers;

    public NovaServers() {
    }

    public List<Server> servers() {
        List<Server> l = new LinkedList<>();
        if (servers != null) {
            l.addAll(servers);
        }

        return l;
    }
}
