package com.cloudbase.opst4j.openstack.domain.compute.action;

import com.cloudbase.opst4j.domain.compute.action.ServerCreate;
import com.cloudbase.opst4j.openstack.domain.compute.NovaNetworkID;
import com.cloudbase.opst4j.openstack.domain.compute.NovaSecurityGroup;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
@JsonRootName("server")
public class NovaServerCreate implements Serializable {

    @JsonProperty("name")
    public String name;

    @JsonProperty("imageRef")
    public String imageRef;

    @JsonProperty("flavorRef")
    public String flavorRef;

    @JsonProperty("max_count")
    public int max_count;

    @JsonProperty("min_count")
    public int min_count;

    @JsonProperty("networks")
    public List<NovaNetworkID> networks;

    @JsonProperty("security_groups")
    public List<NovaSecurityGroup> security_groups;

    public NovaServerCreate() {
    }

    public NovaServerCreate(ServerCreate sc) {
        name = sc.name();
        imageRef = sc.imageRef();
        flavorRef = sc.flavorRef();
        max_count = sc.maxCount();
        min_count = sc.minCount();

        networks = new LinkedList<>();
        for (String s : sc.networks()) {
            networks.add(new NovaNetworkID(s));
        }

        security_groups = new LinkedList<>();
        for (String g : sc.securityGroups()) {
            security_groups.add(new NovaSecurityGroup(g));
        }
    }
}
