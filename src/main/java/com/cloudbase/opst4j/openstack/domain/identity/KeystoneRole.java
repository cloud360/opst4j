package com.cloudbase.opst4j.openstack.domain.identity;

import com.cloudbase.opst4j.domain.identity.Role;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/29/14
 */
public class KeystoneRole implements Role {

    public String name;

    public KeystoneRole(){}

    @Override
    public String name() {
        return name;
    }
}
