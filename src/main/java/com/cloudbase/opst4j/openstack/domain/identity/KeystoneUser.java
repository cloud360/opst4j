package com.cloudbase.opst4j.openstack.domain.identity;

import com.cloudbase.opst4j.domain.identity.Role;
import com.cloudbase.opst4j.domain.identity.User;

import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/29/14
 */
public class KeystoneUser implements User {

    public String username;

    public String id;

    public List<KeystoneRole> roles;

    public String name;

    public KeystoneUser() {
    }

    @Override
    public String username() {
        return username;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public List<Role> roles() {
        List<Role> l = new LinkedList<>();
        l.addAll(this.roles);

        return l;
    }

    @Override
    public String name() {
        return name;
    }
}
