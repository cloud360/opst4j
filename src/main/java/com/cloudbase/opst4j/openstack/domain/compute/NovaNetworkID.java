package com.cloudbase.opst4j.openstack.domain.compute;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
public class NovaNetworkID implements Serializable {

    public String uuid;

    public NovaNetworkID() {
    }

    public NovaNetworkID(String uuid) {
        this.uuid = uuid;
    }
}
