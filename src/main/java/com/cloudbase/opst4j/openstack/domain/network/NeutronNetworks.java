package com.cloudbase.opst4j.openstack.domain.network;

import com.cloudbase.opst4j.domain.network.Network;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/1/14
 */
public class NeutronNetworks implements Serializable {

    public List<NeutronNetwork> networks;

    public NeutronNetworks() {
    }

    public List<Network> networks() {
        List<Network> l = new LinkedList<>();
        if (networks != null) {
            l.addAll(networks);
        }

        return l;
    }
}
