package com.cloudbase.opst4j.openstack.domain.compute;

import com.cloudbase.opst4j.domain.compute.Flavor;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
public class NovaFlavors implements Serializable {

    public List<NovaFlavor> flavors;

    public NovaFlavors(){}

    public List<Flavor> flavors(){
        List<Flavor> l = new LinkedList<>();
        if(flavors != null){
            l.addAll(flavors);
        }

        return l;
    }
}
