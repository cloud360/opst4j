package com.cloudbase.opst4j.openstack.domain.compute;

import com.cloudbase.opst4j.domain.compute.Address;
import com.cloudbase.opst4j.domain.compute.NetworkAddresses;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;

import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
@JsonRootName("network")
public class NovaNetworkAddresses implements NetworkAddresses {

    @JsonProperty("id")
    public String network_label;

    @JsonProperty("ip")
    public List<NovaAddress> nvas;

    public NovaNetworkAddresses() {
    }

    @Override
    public String network_label() {
        return network_label;
    }

    @Override
    public List<Address> ip() {
        List<Address> ads = new LinkedList<>();
        if (nvas != null) {
            ads.addAll(nvas);
        }

        return ads;
    }
}
