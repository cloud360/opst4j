package com.cloudbase.opst4j.openstack.domain.identity;

import com.cloudbase.opst4j.domain.identity.Tenant;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/29/14
 */
public class KeystoneTenant implements Tenant {

    public String description;

    public boolean enabled;

    public String id;

    public String name;

    public KeystoneTenant() {
    }

    @Override
    public String description() {
        return description;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public boolean enabled() {
        return enabled;
    }

    @Override
    public String name() {
        return name;
    }
}
