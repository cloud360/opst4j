package com.cloudbase.opst4j.openstack.domain.identity;

import com.cloudbase.opst4j.domain.identity.Access;
import com.cloudbase.opst4j.domain.identity.Token;
import com.cloudbase.opst4j.domain.identity.User;
import org.codehaus.jackson.map.annotate.JsonRootName;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/29/14
 */
@JsonRootName("access")
public class KeystoneAccess implements Access {

    public KeystoneToken token;

    public KeystoneUser user;

    public KeystoneAccess(){}

    @Override
    public Token token() {
        return token;
    }

    @Override
    public User user() {
        return user;
    }
}
