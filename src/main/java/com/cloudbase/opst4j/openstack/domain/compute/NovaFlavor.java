package com.cloudbase.opst4j.openstack.domain.compute;

import com.cloudbase.opst4j.domain.compute.Flavor;
import org.codehaus.jackson.map.annotate.JsonRootName;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
@JsonRootName("flavor")
public class NovaFlavor implements Flavor {

    public String id;

    public String name;

    public int disk;

    public int ram;

    public int vcpus;

    public NovaFlavor(){}

    @Override
    public String id() {
        return id;
    }

    @Override
    public String name() {
        return name;
    }

    @Override
    public int disk() {
        return disk;
    }

    @Override
    public int ram() {
        return ram;
    }

    @Override
    public int vcpus() {
        return vcpus;
    }
}
