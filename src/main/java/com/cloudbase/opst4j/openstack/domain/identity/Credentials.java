package com.cloudbase.opst4j.openstack.domain.identity;

import org.codehaus.jackson.map.annotate.JsonRootName;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/30/14
 */
@JsonRootName("auth")
public class Credentials implements Serializable {

    public String tenantName;

    public PasswordCredentials passwordCredentials;

    public Credentials() {
    }

    public Credentials(String tenantName, String username, String password) {
        this.tenantName = tenantName;
        this.passwordCredentials = new PasswordCredentials(username, password);
    }

}
