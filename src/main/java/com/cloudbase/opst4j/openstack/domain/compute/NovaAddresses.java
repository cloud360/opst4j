package com.cloudbase.opst4j.openstack.domain.compute;

import com.cloudbase.opst4j.domain.compute.Address;
import com.cloudbase.opst4j.domain.compute.Addresses;
import org.codehaus.jackson.annotate.JsonProperty;

import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/1/14
 */
public class NovaAddresses implements Addresses {

    @JsonProperty("private")
    public List<NovaAddress> privateAddresses;

    @JsonProperty("public")
    public List<NovaAddress> publicAddresses;

    public NovaAddresses() {
    }

    @Override
    public List<Address> privateAddresses() {
        List<Address> l = new LinkedList<>();
        if (privateAddresses != null) {
            l.addAll(privateAddresses);
        }

        return l;
    }

    @Override
    public List<Address> publicAddresses() {
        List<Address> l = new LinkedList<>();
        if (publicAddresses != null) {
            l.addAll(publicAddresses);
        }

        return l;
    }
}
