package com.cloudbase.opst4j.openstack.domain.compute;

import com.cloudbase.opst4j.domain.compute.Image;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/1/14
 */
public class NovaImage implements Image {

    public String id;

    public NovaImage() {
    }

    @Override
    public String id() {
        return id;
    }
}
