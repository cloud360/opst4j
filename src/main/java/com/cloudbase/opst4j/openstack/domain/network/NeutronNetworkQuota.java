package com.cloudbase.opst4j.openstack.domain.network;

import com.cloudbase.opst4j.domain.network.NetworkQuota;
import org.codehaus.jackson.map.annotate.JsonRootName;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
@JsonRootName("quota")
public class NeutronNetworkQuota implements NetworkQuota {

    public int network;

    public int port;

    public int router;

    public int subnet;

    public int floatingip;

    public NeutronNetworkQuota(){}
    
    @Override
    public int network() {
        return network;
    }

    @Override
    public int port() {
        return port;
    }

    @Override
    public int router() {
        return router;
    }

    @Override
    public int subnet() {
        return subnet;
    }

    @Override
    public int floatingip() {
        return floatingip;
    }
}
