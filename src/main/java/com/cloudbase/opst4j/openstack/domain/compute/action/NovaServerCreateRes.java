package com.cloudbase.opst4j.openstack.domain.compute.action;

import com.cloudbase.opst4j.domain.compute.action.ServerCreateRes;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/5/14
 */
public class NovaServerCreateRes implements ServerCreateRes {

    private int status;

    private String message;

    public NovaServerCreateRes(int status, String message) {
        this.status = status;
        this.message = message;
    }

    @Override
    public int status() {
        return status;
    }

    @Override
    public String message() {
        return message;
    }
}
