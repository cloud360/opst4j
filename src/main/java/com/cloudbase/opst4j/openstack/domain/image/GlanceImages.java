package com.cloudbase.opst4j.openstack.domain.image;

import com.cloudbase.opst4j.domain.image.Image;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/31/14
 */
public class GlanceImages implements Serializable {

    public List<GlanceImage> images;

    public GlanceImages() {
    }

    public List<Image> images() {
        List<Image> l = new LinkedList<>();
        if (images != null) {
            l.addAll(images);
        }

        return l;
    }
}
