package com.cloudbase.opst4j.openstack.domain.network;

import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
public class NeutronSecureGroups {

    public List<NeutronSecureGroup> security_groups;
}
