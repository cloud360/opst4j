package com.cloudbase.opst4j.openstack.domain.identity;

import com.cloudbase.opst4j.domain.identity.Tenant;
import com.cloudbase.opst4j.domain.identity.Token;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/29/14
 */
public class KeystoneToken implements Token {

    public String issued_at;

    public String expires;

    public String id;

    public KeystoneTenant tenant;

    public KeystoneToken() {
    }

    @Override
    public String issued_at() {
        return issued_at;
    }

    @Override
    public String expires() {
        return expires;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public Tenant tenant() {
        return tenant;
    }
}
