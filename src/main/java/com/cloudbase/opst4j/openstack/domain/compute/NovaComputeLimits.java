package com.cloudbase.opst4j.openstack.domain.compute;

import com.cloudbase.opst4j.domain.compute.AbsoluteLimit;
import com.cloudbase.opst4j.domain.compute.ComputeLimits;
import org.codehaus.jackson.map.annotate.JsonRootName;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
@JsonRootName("limits")
public class NovaComputeLimits implements ComputeLimits {

    public NovaAbsoluteLimit absolute;

    @Override
    public AbsoluteLimit absolute() {
        return absolute;
    }
}
