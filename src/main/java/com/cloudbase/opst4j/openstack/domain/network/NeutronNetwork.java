package com.cloudbase.opst4j.openstack.domain.network;

import com.cloudbase.opst4j.domain.network.Network;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;

import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/1/14
 */
@JsonRootName("network")
public class NeutronNetwork implements Network {

    public String name;

    public String id;

    public String tenant_id;

    @JsonProperty("router:external")
    public boolean external_router;

    @JsonProperty("provider:network_type")
    public String network_type_provider;

    @JsonProperty("provider:physical_network")
    public String physical_network_provider;

    @JsonProperty("provider:segmentation_id")
    public String segmentation_id_provider;

    public boolean shared;

    public boolean admin_state_up;

    public List<String> subnets;

    public String status;

    public NeutronNetwork(){}

    @Override
    public String name() {
        return name;
    }

    @Override
    public String network_type_provider() {
        return network_type_provider;
    }

    @Override
    public String physical_network_provider() {
        return physical_network_provider;
    }

    @Override
    public String id() {
        return id;
    }

    @Override
    public String tenant_id() {
        return tenant_id;
    }

    @Override
    public boolean external_router() {
        return external_router;
    }

    @Override
    public boolean shared() {
        return shared;
    }

    @Override
    public String segmentation_id_provider() {
        return segmentation_id_provider;
    }

    @Override
    public boolean admin_state_up() {
        return admin_state_up;
    }

    @Override
    public String status() {
        return status;
    }

    @Override
    public List<String> subnets() {
        return subnets;
    }
}
