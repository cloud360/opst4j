package com.cloudbase.opst4j.api.block;

import com.cloudbase.opst4j.domain.block.Volume;

import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
public interface VolumeService {

    public List<Volume> listVolumes(String tenantId, String access_token);

    public Volume detailVolume(String tenantId, String volumeId, String access_token);
}
