package com.cloudbase.opst4j.api.image;

import com.cloudbase.opst4j.domain.image.Image;

import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/31/14
 */
public interface ImageService {

    public List<Image> listImages(String access_token);

    public Image imageDetail(String id, String access_token);
}
