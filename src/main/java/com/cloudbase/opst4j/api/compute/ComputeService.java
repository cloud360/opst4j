package com.cloudbase.opst4j.api.compute;

import com.cloudbase.opst4j.api.image.ImageService;
import com.cloudbase.opst4j.api.network.NetworkService;
import com.cloudbase.opst4j.domain.compute.ComputeLimits;
import com.cloudbase.opst4j.domain.compute.Flavor;
import com.cloudbase.opst4j.domain.compute.NetworkAddresses;
import com.cloudbase.opst4j.domain.compute.Server;
import com.cloudbase.opst4j.domain.compute.action.ServerCreate;
import com.cloudbase.opst4j.domain.compute.action.ServerCreateRes;
import com.cloudbase.opst4j.domain.compute.action.ServerRebuild;

import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/28/14
 */
public interface ComputeService {

    ImageService imageService();

    NetworkService networkService();

    List<Server> detailServers(String tenantId, String access_token);

    Server detailServer(String tenantId, String server_id, String access_token);

    ComputeLimits limits(String tenantId, String access_token);

    List<Flavor> detailFlavors(String tenantId, String access_token);

    Flavor detailFlavor(String tenantId, String flavor_id, String access_token);

    NetworkAddresses networkAddresses(String tenantId, String serverId, String networkLabel, String access_token);

    ServerCreateRes createServer(String tenantId, String access_token, ServerCreate sc);

    void rebootServer(String tenantId, String serverId, String access_token);

    Server rebuildServer(String tenantId, String serverId, String access_token, ServerRebuild sr);

    void resizeServer(String tenantId, String serverId, String flavorRef, String access_token);

    void changePassword(String tenantId, String serverId, String newPass, String acces_token);

    void deleteServer(String tenantId, String serverId, String access_token);
}
