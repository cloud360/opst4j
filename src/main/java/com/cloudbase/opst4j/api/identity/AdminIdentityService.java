package com.cloudbase.opst4j.api.identity;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/9/14
 */
public interface AdminIdentityService {

    int createUser(String username, String password, String admin_token);
}
