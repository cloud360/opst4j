package com.cloudbase.opst4j.api.identity;

import com.cloudbase.opst4j.domain.identity.Access;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/28/14
 */
public interface IdentityService {

    Access token(String tenant, String username, String password);

    Access refreshToken(String tenant, String current_token);

}
