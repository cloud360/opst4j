package com.cloudbase.opst4j.api.network;

import com.cloudbase.opst4j.domain.network.Network;
import com.cloudbase.opst4j.domain.network.NetworkQuota;
import com.cloudbase.opst4j.domain.network.SecureGroup;

import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/1/14
 */
public interface NetworkService {

    public List<Network> listNetworks(String access_token);

    public Network detailNetwork(String id, String access_token);

    public NetworkQuota quotas(String access_token);

    public List<SecureGroup> listSecureGroups(String access_token);

    public SecureGroup detailSecureGroup(String secGroupId, String access_token);
}
