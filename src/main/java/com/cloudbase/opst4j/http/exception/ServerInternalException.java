package com.cloudbase.opst4j.http.exception;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/5/14
 */
public class ServerInternalException extends RuntimeException {

    public ServerInternalException(String message) {
        super(message);
    }
}
