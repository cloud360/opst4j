package com.cloudbase.opst4j.http;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/29/14
 */
public interface HttpExecutor {

    public <R> R execute(HttpCommand<R> command, Object entity);
}
