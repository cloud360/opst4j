package com.cloudbase.opst4j.http;

import org.codehaus.jackson.map.DeserializationConfig;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.SerializationConfig;
import org.codehaus.jackson.map.annotate.JsonRootName;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import javax.ws.rs.ext.ContextResolver;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/30/14
 */
public class JacksonMapperResolver implements ContextResolver<ObjectMapper> {

    private ObjectMapper mapper;

    private ObjectMapper topMapper;

    public JacksonMapperResolver() {

        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        mapper.disable(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES);

        topMapper = new ObjectMapper();
        topMapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_NULL);
        topMapper.disable(DeserializationConfig.Feature.FAIL_ON_UNKNOWN_PROPERTIES);
        topMapper.enable(DeserializationConfig.Feature.UNWRAP_ROOT_VALUE);
        topMapper.enable(SerializationConfig.Feature.WRAP_ROOT_VALUE);
    }

    @Override
    public ObjectMapper getContext(Class<?> type) {
        return type.getAnnotation(JsonRootName.class) == null ? mapper : topMapper;
    }
}
