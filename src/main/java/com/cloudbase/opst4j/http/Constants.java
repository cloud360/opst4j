package com.cloudbase.opst4j.http;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/30/14
 */
public class Constants {

    public final static String X_AUTH_TOKEN = "X-Auth-Token";

    public final static String CONTENT_TYPE = "Content-Type";

    public final static String ACCEPT = "Accept";
}
