package com.cloudbase.opst4j.domain.identity;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/29/14
 */
public interface Access extends Serializable {

    Token token();

    User user();
}
