package com.cloudbase.opst4j.domain.identity;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/29/14
 */
public interface User extends Serializable {

    String username();

    String id();

    List<Role> roles();

    String name();
}
