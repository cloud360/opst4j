package com.cloudbase.opst4j.domain.image;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/31/14
 */
public interface Image extends Serializable {

    String status();

    String name();

    String id();

    String created_at();

    String updated_at();

    String deleted_at();

    String disk_format();

    long min_disk();

    long min_ram();

    String checksum();

    String owner();

    String file();

    String visibility();

    String self();

    boolean isProtected();

    long size();

    String schema();
}
