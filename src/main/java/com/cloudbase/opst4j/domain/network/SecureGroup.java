package com.cloudbase.opst4j.domain.network;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
public interface SecureGroup extends Serializable {

    String id();

    String description();

    String name();

    List<SecureGroupRule> secureGroupRules();

    String tenant_id();

}
