package com.cloudbase.opst4j.domain.network;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
public interface NetworkQuota extends Serializable {

    int subnet();

    int router();

    int port();

    int network();

    int floatingip();
}
