package com.cloudbase.opst4j.domain.network;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
public interface SecureGroupRule extends Serializable {

    String id();

    String direction();

    String ethertype();

    int port_range_max();

    int port_range_min();

    String protocol();

    String remote_group_id();

    String remote_ip_prefix();

    String security_group_id();

    String tenant_id();
}
