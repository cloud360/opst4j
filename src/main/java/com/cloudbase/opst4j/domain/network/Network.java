package com.cloudbase.opst4j.domain.network;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/1/14
 */
public interface Network extends Serializable {

    String status();

    List<String> subnets();

    String name();

    String physical_network_provider();

    boolean admin_state_up();

    String tenant_id();

    String network_type_provider();

    boolean external_router();

    boolean shared();

    String id();

    String segmentation_id_provider();

}
