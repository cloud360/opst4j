package com.cloudbase.opst4j.domain.compute;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
public interface AbsoluteLimit extends Serializable {

    int maxImageMeta();

    int maxPersonality();

    long maxPersonalitySize();

    int maxSecurityGroupRules();

    int maxSecurityGroups();

    int maxServerMeta();

    int maxTotalCores();

    int maxTotalFloatingIps();

    int maxTotalInstances();

    int maxTotalKeypairs();

    long maxTotalRAMSize();

}
