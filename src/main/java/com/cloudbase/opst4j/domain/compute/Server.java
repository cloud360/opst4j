package com.cloudbase.opst4j.domain.compute;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 10/30/14
 */
public interface Server extends Serializable {

    String accessIPv4();

    String accessIPv6();

    String created();

    String hostId();

    String id();

    String name();

    int progress();

    String status();

    String tenant_id();

    String updated();

    String user_id();

    Addresses addresses();

    Image image();

    Flavor flavor();
}
