package com.cloudbase.opst4j.domain.compute;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
public interface NetworkAddresses extends Serializable {

    String network_label();

    List<Address> ip();

}
