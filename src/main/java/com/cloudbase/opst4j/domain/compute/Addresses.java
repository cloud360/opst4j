package com.cloudbase.opst4j.domain.compute;

import java.io.Serializable;
import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/1/14
 */
public interface Addresses extends Serializable {

    List<Address> privateAddresses();

    List<Address> publicAddresses();

}
