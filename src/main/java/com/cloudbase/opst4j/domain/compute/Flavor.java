package com.cloudbase.opst4j.domain.compute;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
public interface Flavor extends Serializable {

    String id();

    String name();

    int disk();

    int ram();

    int vcpus();

}
