package com.cloudbase.opst4j.domain.compute.action;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
public interface ServerResize extends Serializable {
}
