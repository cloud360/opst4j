package com.cloudbase.opst4j.domain.compute;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/1/14
 */
public interface Address extends Serializable {

    String addr();

    int version();

}
