package com.cloudbase.opst4j.domain.compute.action;

import java.util.List;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/4/14
 */
public interface ServerCreate {

    String name();

    String imageRef();

    String flavorRef();

    int maxCount();

    int minCount();

    List<String> networks();

    List<String> securityGroups();
}
