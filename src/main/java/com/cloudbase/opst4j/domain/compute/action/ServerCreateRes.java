package com.cloudbase.opst4j.domain.compute.action;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/5/14
 */
public interface ServerCreateRes {

    int status();

    String message();
}
