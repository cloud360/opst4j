package com.cloudbase.opst4j.domain.block;

import java.io.Serializable;

/**
 * @author <a href="hoang281283@gmail.com">Minh Hoang TO</a>
 * @date: 11/2/14
 */
public interface Volume extends Serializable {

    String id();

    String name();

    String description();

    String created_at();

    long size();

    String status();

    String availability_zone();

    String bootable();

    String os_vol_host();

    String os_vol_tenant();

    String volume_type();
}
